
import React from 'react'

export function Toggle(props) {

    return (
        <div>
            <h1> {props.isDisabled ? "OFF" : "ON"}</h1>
            <button onClick={props.onPress}>    
                {props.isDisabled ? "Turn on" : "Turn off"}
            </button>
        </div>
    )

}