import React from 'react';

export function Counter(props) {

   return (
      <div>
        <p>Button Count: {props.counter}</p>
        <button onClick={props.increment}>
         Increase
        </button>
      </div> 
    );
  }