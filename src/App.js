import './App.css';
import React, { useState } from 'react';
import {Counter} from './component/counter'
import {Toggle} from './component/toggle'

function App() {
  const [counter, setCounter] = useState(0);
  const increment = () => setCounter(counter+1)

  const [toggle, setToggle] = useState(false)
  const handleClick = () => setToggle(!toggle)

  const students = [
    {
        name: "Iqfar",
        age: 20
    },
    {
        name: "Udhin",
        age: 23
    },
    {
        name: "Ary",
        age: 22
    },
    {
        name: "Krisna",
        age: 26
    },
    {
        name: "Afnur",
        age: 22
    }
  ]
 

  return (
    <div style={{flexDirection: 'row'}} className="App">
        <Counter counter={counter} increment={increment} />
        <Toggle isDisabled={toggle} onPress={handleClick}/>
        <h3 style={{textAlign: 'left', paddingLeft: 20}}>Data Students</h3>
        <ul>
        {students.map((student, i) => 
          <li style={{textAlign: 'left'}}>{student.name} {student.age}</li>
        )}
        </ul>
    </div>
  );
}

export default App;
